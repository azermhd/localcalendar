package com.azermehdibeyli.www.localcalendar;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by azer.mehdibeyli on 13.12.2017.
 */

public class PagerViewAdapter extends PagerAdapter {

    private AppCompatActivity appCompatActivity;
    private Context context;
    private Date[] dateList;

    public PagerViewAdapter(AppCompatActivity appCompatActivity, Date[] dateList) {
        this.appCompatActivity = appCompatActivity;
        this.context = appCompatActivity.getApplicationContext();
        this.dateList = dateList;
    }

    private class ViewHolder {
        ScheduleGridViewItems gridViewItems;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.pager_view_page, collection, false);
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.gridViewItems = new ScheduleGridViewItems();
        viewHolder.gridViewItems.gridView = layout.findViewById(R.id.main_gridView);
        viewHolder.gridViewItems.tvGridHeader = layout.findViewById(R.id.main_gridTitle);
        viewHolder.gridViewItems.weekNumbersLayout = layout.findViewById(R.id.main_weekNumbersLayout);
        viewHolder.gridViewItems.mainParentLayout = layout.findViewById(R.id.main_gridParentLayout);

        ScheduleGridController scheduleGridController = new ScheduleGridController(appCompatActivity, viewHolder.gridViewItems);
        scheduleGridController.setViewItems(dateList[position]);
        collection.addView(layout);

        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return dateList.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "dsdassd" + position;
    }
}
