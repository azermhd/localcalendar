package com.azermehdibeyli.www.localcalendar;

import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ScheduleGridViewItems {
    public GridView gridView;
    public TextView tvGridHeader;
    public LinearLayout weekNumbersLayout;
    public RelativeLayout mainParentLayout;

    public ScheduleGridViewItems() {

    }
}
