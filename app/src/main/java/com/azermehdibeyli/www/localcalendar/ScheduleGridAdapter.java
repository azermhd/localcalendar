package com.azermehdibeyli.www.localcalendar;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Azer on 26.11.2017.
 */

public class ScheduleGridAdapter extends BaseAdapter {

    private Context mContext;
    private final DayOfSchedule[] daysOfSchedule;
    private int cellImageWidth;

    public ScheduleGridAdapter(Context mContext, DayOfSchedule[] daysOfSchedule, int cellImageWidth) {
        this.mContext = mContext;
        this.daysOfSchedule = daysOfSchedule;
        this.cellImageWidth = cellImageWidth;
    }

    private class ViewHolder {
        TextView day;
        RelativeLayout dayTypeImageParent;
    }

    @Override
    public int getCount() {
        return daysOfSchedule.length;
    }

    @Override
    public Object getItem(int position) {
        return daysOfSchedule[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.schedule_grid_cell, null);
            holder = new ViewHolder();
            holder.day = convertView.findViewById(R.id.schedule_grid_cell_day);
            holder.dayTypeImageParent = convertView.findViewById(R.id.schedule_grid_cell_image_parent);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.day.setText(daysOfSchedule[position].getDay() + "");
        holder.day.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) (cellImageWidth / 2.12f));
        LinearLayout.LayoutParams dayTypeImageParams = new LinearLayout.LayoutParams(cellImageWidth, cellImageWidth);
        holder.dayTypeImageParent.setLayoutParams(dayTypeImageParams);

        switch (daysOfSchedule[position].getDayType()) {
            case DEFAULT:
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.day_default);
                holder.day.setTextColor(mContext.getResources().getColor(R.color.lightGrey));
                break;

            case WORKDAY:
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.workday);
                break;
            case WORKDAY_HOLIDAY:
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.workday_holiday);
                break;
            case WORKDAY_SHIFTED_HOLIDAY:
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.workday_shifted_holiday);
                break;
            case TODAY_WORKDAY_SHIFTED_HOLIDAY:
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.workday_shifted_holiday_today);
                break;
            case TODAY_WORKDAY_HOLIDAY:
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.workday_holiday_today);
                break;
            case TODAY_WORKDAY:
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.workday_today);
                break;

            case MOURNING:
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.mourning);
                break;
            case TODAY_MOURNING:
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.mourning_today);
                break;

            case WEEKEND:
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.weekend);
                break;
            case WEEKEND_HOLIDAY:
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.weekend_holiday);
                break;
            case TODAY_WEEKEND:
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.weekend_today);
                break;
            case TODAY_WEEKEND_HOLIDAY:
                holder.dayTypeImageParent.setBackgroundResource(R.drawable.weekend_holiday_today);
                break;
        }

        return convertView;
    }
}
