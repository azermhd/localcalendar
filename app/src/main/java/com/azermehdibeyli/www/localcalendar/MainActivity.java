package com.azermehdibeyli.www.localcalendar;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    ScheduleGridController scheduleGridController;
    public Date selectedMonthCommon;
    private ViewPager viewPager;

    int pageCount = 3;
    Date[] dateList;

    public MainActivity() {
        dateList = getDateList(pageCount, Calendar.getInstance().getTime());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeObjects();
        setListeners();

        viewPager = findViewById(R.id.testViewPager);
        final PagerViewAdapter pagerViewAdapter = new PagerViewAdapter(this, dateList);
        viewPager.setAdapter(pagerViewAdapter);
        viewPager.setCurrentItem(pageCount/2, false);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    int centerIndex = pageCount / 2;
                    int difference = viewPager.getCurrentItem() - centerIndex;
                    Calendar calendarFirstDate = Calendar.getInstance();
                    calendarFirstDate.setTime(dateList[centerIndex]);
                    calendarFirstDate.add(Calendar.MONTH, difference);
                    dateList = getDateList(pageCount, calendarFirstDate.getTime());
                    PagerViewAdapter pagerViewAdapter = new PagerViewAdapter(MainActivity.this, dateList);
                    viewPager.setAdapter(pagerViewAdapter);
                    // go back to the center allowing to scroll indefinitely
                    viewPager.setCurrentItem(pageCount / 2, false);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        Log.i("onResume", "onResume");
        PagerViewAdapter pagerViewAdapter = new PagerViewAdapter(this, dateList);
        viewPager.setAdapter(pagerViewAdapter);
        viewPager.setCurrentItem(pageCount / 2, false);
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void initializeObjects() {
        ScheduleGridViewItems scheduleGridViewItems = new ScheduleGridViewItems();
        scheduleGridViewItems.gridView = findViewById(R.id.main_gridView);
        scheduleGridViewItems.tvGridHeader = findViewById(R.id.main_gridTitle);
        scheduleGridViewItems.weekNumbersLayout = findViewById(R.id.main_weekNumbersLayout);

        this.scheduleGridController = new ScheduleGridController(this, scheduleGridViewItems);
    }

    private void setListeners() {
    }

    private Date[] getDateList(int pageCount, Date centerDate) {
        if (pageCount > 0) {
            if (pageCount % 2 == 0) {
                pageCount++;
            }

            Calendar calendarFirstDate = Calendar.getInstance();
            calendarFirstDate.setTime(centerDate);
            calendarFirstDate.set(Calendar.DAY_OF_MONTH, 1);
            calendarFirstDate.set(Calendar.HOUR_OF_DAY, 0);
            calendarFirstDate.set(Calendar.MINUTE, 0);
            calendarFirstDate.set(Calendar.SECOND, 0);
            calendarFirstDate.set(Calendar.MILLISECOND, 0);
            calendarFirstDate.add(Calendar.MONTH, -pageCount / 2);

            Date[] dateList = new Date[pageCount];

            for (int i = 0; i < pageCount; i++) {
                dateList[i] = calendarFirstDate.getTime();
                calendarFirstDate.add(Calendar.MONTH, 1);
            }

            return dateList;
        } else {
            return new Date[0];
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private boolean addSwipeTrainerIfNeeded() {
        /*
        swipeTrainingLayout = findViewById(R.id.main_swipeTrainingLayout);
        if (swipeTrainingLayout != null)
            mainParentLayout.removeView(swipeTrainingLayout);

        if (!settings.isSwipeTrained() && !settings.isAllDatesAdjusted()) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int tenDp = Math.round(displayMetrics.density * 10);

            swipeTrainingLayout = new RelativeLayout(this);
            swipeTrainingLayout.setId(R.id.main_swipeTrainingLayout);
            swipeTrainingLayout.setBackgroundResource(R.drawable.white_rounded_backgroud_alpha);
            swipeTrainingLayout.setPadding(tenDp, tenDp, tenDp, tenDp);
            RelativeLayout.LayoutParams swipeTrainingLayoutLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            swipeTrainingLayout.setLayoutParams(swipeTrainingLayoutLayoutParams);

            ImageView swipeImageView = new ImageView(this);
            swipeImageView.setBackgroundResource(R.mipmap.swipe_arrows);
            swipeImageView.setScaleType(ImageView.ScaleType.CENTER);
            swipeImageView.setId(R.id.main_swipeTrainingImage);

            RelativeLayout.LayoutParams swipeImageViewLayoutParams = new RelativeLayout.LayoutParams((int) (displayMetrics.widthPixels / 2.3), (int) (displayMetrics.widthPixels / 2.3));
            swipeImageViewLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
            swipeImageViewLayoutParams.setMargins(0, (int) (displayMetrics.heightPixels / 5.9), 0, (int) (30 * displayMetrics.density));
            swipeImageView.setLayoutParams(swipeImageViewLayoutParams);

            TextView swipeTrainingTextView = new TextView(getApplicationContext());
            swipeTrainingTextView.setId(R.id.main_swipeTrainingText);
            swipeTrainingTextView.setText(this.getString(R.string.mainSwipeTrainingTextText));
            swipeTrainingTextView.setTextColor(getResources().getColor(R.color.mainBackColorDark));
            swipeTrainingTextView.setBackgroundResource(R.drawable.white_rounded_backgroud);
            swipeTrainingTextView.setPadding(tenDp, tenDp, tenDp, tenDp);
            swipeTrainingTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, displayMetrics.widthPixels / 25);
            swipeTrainingTextView.setText(R.string.mainSwipeTrainingTextText);

            RelativeLayout.LayoutParams swipeTrainingTextViewLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            swipeTrainingTextViewLayoutParams.addRule(RelativeLayout.BELOW, R.id.main_swipeTrainingImage);
            swipeTrainingTextViewLayoutParams.setMargins(0, 0, 0, tenDp);
            swipeTrainingTextView.setLayoutParams(swipeTrainingTextViewLayoutParams);

            Button swipeTrainingButton = new Button(this);
            swipeTrainingButton.setId(R.id.main_swipeTrainingButton);
            swipeTrainingButton.setBackgroundResource(R.drawable.save_button_background);
            swipeTrainingButton.setText("GOT IT");
            swipeTrainingButton.setTextColor(getResources().getColor(R.color.white));

            RelativeLayout.LayoutParams swipeTrainingButtonLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            swipeTrainingButtonLayoutParams.addRule(RelativeLayout.BELOW, R.id.main_swipeTrainingText);
            swipeTrainingButtonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            swipeTrainingButton.setLayoutParams(swipeTrainingButtonLayoutParams);

            swipeTrainingLayout.addView(swipeImageView);
            swipeTrainingLayout.addView(swipeTrainingTextView);
            swipeTrainingLayout.addView(swipeTrainingButton);

            mainParentLayout.addView(swipeTrainingLayout);

            swipeTrainingButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainParentLayout.removeView(swipeTrainingLayout);
                    settings.setSwipeTrained(true);
                    addNotificationsIfNeeded();
                }
            });
            return true;
        } else if (!settings.isSwipeTrained() && settings.isAllDatesAdjusted()) {
            settings.setSwipeTrained(true);
        }
*/
        return false;

    }

}

