package com.azermehdibeyli.www.localcalendar;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Azer on 04.03.2018.
 */

public class ScheduleGridController {
    private ScheduleGridViewItems viewItems;
    private AppCompatActivity appCompatActivity;

    public ViewSettings viewSettings;
    public ScheduleGridData scheduleGridDataMethods;

    public DateFormat dateFormatForGridHeader;

    public ScheduleGridController(AppCompatActivity appCompatActivity, ScheduleGridViewItems viewItems) {
        this.appCompatActivity = appCompatActivity;
        this.viewSettings = new ViewSettings(appCompatActivity);
        this.scheduleGridDataMethods = new ScheduleGridData();
        this.dateFormatForGridHeader = new SimpleDateFormat("MMMM yyyy");
        this.viewItems = viewItems;
    }

    public void setViewItems(Date selectedMonth){
        setAdapterToScheduleGrid(selectedMonth);
        viewItems.tvGridHeader.setText(dateFormatForGridHeader.format(selectedMonth));
        adjustViewDimens();
    }

    private void adjustViewDimens() {
        int cellImageWidth = viewSettings.getGridCellW();
        //tvShiftName.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) (cellImageWidth / 2.50f));
        viewItems.tvGridHeader.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) (cellImageWidth / 1.89f));

        final int childCount = viewItems.weekNumbersLayout.getChildCount();
        for (int i = 0; i < childCount; i++) {
            TextView textView = (TextView) viewItems.weekNumbersLayout.getChildAt(i);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int) (cellImageWidth / 2.80f));
        }
    }

    private void setAdapterToScheduleGrid(Date dateSelectedMonth) {
        ScheduleGridAdapter adapter = new ScheduleGridAdapter(appCompatActivity,
                scheduleGridDataMethods.getDaysOfSchedule(dateSelectedMonth),
                viewSettings.getGridCellW());
        viewItems.gridView.setAdapter(adapter);
    }
}
