package com.azermehdibeyli.www.localcalendar;

/**
 * Created by Azer on 26.11.2017.
 */

public enum DayType {
    WEEKEND,
    WEEKEND_HOLIDAY,
    WORKDAY,
    WORKDAY_HOLIDAY,
    WORKDAY_SHIFTED_HOLIDAY,
    MOURNING,
    DEFAULT,
    TODAY_WEEKEND,
    TODAY_WEEKEND_HOLIDAY,
    TODAY_WORKDAY_HOLIDAY,
    TODAY_WORKDAY_SHIFTED_HOLIDAY,
    TODAY_WORKDAY,
    TODAY_MOURNING,
}
